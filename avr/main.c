/* Name: main.c
 * Project: hid-data, example how to use HID for data transfer
 * Author: Christian Starkjohann
 * Creation Date: 2008-04-11
 * Tabsize: 4
 * Copyright: (c) 2008 by OBJECTIVE DEVELOPMENT Software GmbH
 * License: GNU GPL v2 (see License.txt), GNU GPL v3 or proprietary (CommercialLicense.txt)
 */

/*
This example should run on most AVRs with only little changes. No special
hardware resources except INT0 are used. You may have to change usbconfig.h for
different I/O pins for USB. Please note that USB D+ must be the INT0 pin, or
at least be connected to INT0 as well.
*/

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>  /* for sei() */
#include <util/delay.h>     /* for _delay_ms() */
#include <avr/eeprom.h>

#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include <string.h>
#include "usbdrv.h"
#include "oddebug.h"        /* This is also an example for using debug macros */
#include "irmp.h"

// default 24h time format 
#define HOUR24

#define MODE_TIME   0
#define  MODE_RAWUSB 1

typedef struct {
// 0x00
    uint8_t ir_data;
// 0x01    
    uint16_t ir_count;
// 0x03    
    uint8_t cmd;
// 0x04    
    uint8_t params[16];
// 0x14
    uint8_t rawdata[16];
// 0x24   
    uint8_t ircodes[16];
} __attribute__ ((packed)) t_usbdata;

union u_usbdata {
    t_usbdata data;
    uchar raw[128];
};

union u_usbdata usbdata;

volatile uint8_t usbreceived=0;


uint8_t ir_code = 0;
uint16_t ir_count = 0;
uint8_t ir_codes[11]={0,0,0,0,0,0,0,0,0,0,0};

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */

PROGMEM const char usbHidReportDescriptor[22] = {    /* USB report descriptor */
    0x06, 0x00, 0xff,              // USAGE_PAGE (Generic Desktop)
    0x09, 0x01,                    // USAGE (Vendor Usage 1)
    0xa1, 0x01,                    // COLLECTION (Application)
    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
    0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
    0x75, 0x08,                    //   REPORT_SIZE (8)
    0x95, 0x80,                    //   REPORT_COUNT (128)
    0x09, 0x00,                    //   USAGE (Undefined)
    0xb2, 0x02, 0x01,              //   FEATURE (Data,Var,Abs,Buf)
    0xc0                           // END_COLLECTION
};
/* Since we define only one feature report, we don't use report-IDs (which
 * would be the first byte of the report). The entire report consists of 128
 * opaque data bytes.
 */

/* The following variables store the status of the current data transfer */
static uchar    currentAddress;
static uchar    bytesRemaining;



/* ------------------------------------------------------------------------- */

/* usbFunctionRead() is called when the host requests a chunk of data from
 * the device. For more information see the documentation in usbdrv/usbdrv.h.
 */
uchar   usbFunctionRead(uchar *data, uchar len)
{
    if(len > bytesRemaining)
        len = bytesRemaining;

    int i;
    // inject data to be read like ir codes
    usbdata.raw[0] = ir_code;
    usbdata.raw[1] = ir_count & 0xff;
    usbdata.raw[2] = (ir_count & 0xff00) >> 8;

    uint8_t j;
    for (j=0;j<11;j++)
        usbdata.data.ircodes[j] = ir_codes[j];
    
    for(i = 0; i < len; i++) {
        if (currentAddress+i < sizeof(usbdata.raw)) {
            data[i] = usbdata.raw[currentAddress+i];
        }
    }

    currentAddress += len;
    bytesRemaining -= len;

    return len;
}

/* usbFunctionWrite() is called when the host sends a chunk of data to the
 * device. For more information see the documentation in usbdrv/usbdrv.h.
 */
uchar   usbFunctionWrite(uchar *data, uchar len)
{
    if(bytesRemaining == 0)
        return 1;               /* end of transfer */
    if(len > bytesRemaining)
        len = bytesRemaining;

    int i;
    for(i = 0; i < len; i++)
        if (currentAddress+i < sizeof(usbdata.raw))
            usbdata.raw[currentAddress+i] = data[i];


    currentAddress += len;
    bytesRemaining -= len;
    
    if (!bytesRemaining)
        usbreceived=1;
        
    
    return bytesRemaining == 0; /* return 1 if this was the last chunk */
    // command decoder ...
}


/* ------------------------------------------------------------------------- */

usbMsgLen_t usbFunctionSetup(uchar data[8])
{
usbRequest_t    *rq = (void *)data;

    if((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS){    /* HID class request */
        if(rq->bRequest == USBRQ_HID_GET_REPORT){  /* wValue: ReportType (highbyte), ReportID (lowbyte) */
            /* since we have only one report type, we can ignore the report-ID */
            bytesRemaining = 128;
            currentAddress = 0;
            return USB_NO_MSG;  /* use usbFunctionRead() to obtain data */
        }else if(rq->bRequest == USBRQ_HID_SET_REPORT){
            /* since we have only one report type, we can ignore the report-ID */
            bytesRemaining = 128;
            currentAddress = 0;
            return USB_NO_MSG;  /* use usbFunctionWrite() to receive data from host */
        }
    }else{
        /* ignore vendor type requests, we don't use any */
    }
    return 0;
}

/* ------------------------------------------------------------------------- */

// Port Pin Definitions

// PORTD
#define PD_SEGF (1<<0)
#define PD_SEGA (1<<1)

// PD_FILDRV    (1<<4)   V3
#define PD_FILDRV    (1<<5)

// PD_GATE1 (1<<5) V3
#define PD_GATE1 (1<<6) 
#define PD_GATE3 (1<<4)

#define PD_GATE8 (1<<7)

// PORTC
#define PC_SEGDP (1<<1)
#define PC_SEGD (1<<0)
#define PC_SEGC (1<<2)
#define PC_SEGE (1<<3)
#define PC_SEGG (1<<5)
#define PC_SEGB (1<<4)

// PORTB
#define PB_GATE5 (1<<0)
#define PB_GATE6 (1<<1)
#define PB_GATE7 (1<<2)
#define PB_IR    (1<<3)
#define PB_GATE2 (1<<4)
#define PB_GATE4 (1<<5)
#define PB_GATE9    (1<<7)


// Bit encoding in rawdata

#define SEGA (1<<0)
#define SEGB (1<<1)
#define SEGC (1<<2)
#define SEGD (1<<3)
#define SEGE (1<<4)
#define SEGF (1<<5)
#define SEGG (1<<6)
#define SEGDP (1<<7)

// raw segment data definitions for numbers, cursor, dash and point
uint16_t digit[20]={
    SEGA | SEGB | SEGC | SEGD | SEGE | SEGF,
    SEGB | SEGC,
    SEGA | SEGB | SEGG | SEGE | SEGD,
    SEGA | SEGB | SEGC | SEGD | SEGG,
    SEGF | SEGG | SEGB | SEGC,
    SEGA | SEGF | SEGG | SEGC | SEGD,
    SEGA | SEGF | SEGE | SEGD | SEGC | SEGG,
    SEGA | SEGB | SEGC,
    SEGA | SEGB | SEGC | SEGD | SEGE | SEGF | SEGG,
    SEGA | SEGB | SEGC | SEGD | SEGF | SEGG,
    SEGA | SEGB | SEGC | SEGE | SEGF | SEGG,    // 0xa
    SEGA | SEGB | SEGC | SEGD | SEGE | SEGF | SEGG, // 0xb
    SEGA | SEGB | SEGC | SEGD, // 0xc
    SEGA | SEGB | SEGC | SEGD | SEGE | SEGF, // 0xd
    SEGA | SEGF | SEGE | SEGD | SEGG, // 0xe
    SEGA | SEGF | SEGE | SEGG, // 0xf
    SEGD,    // cursor  0x10
    SEGG,    // dash 0x11
    0, // empty 0x12
    SEGDP,  // point
};

// default display "  -  -  "
uint16_t display[9]={0,0,0x11,0,0,0x11,0,0,0x13};

// array contains rawdata which is set per USB or calculated
// from display-data
uint16_t rawdata[9]={0,0,0,0,0,0,0,0,0};


volatile uint8_t flag = 0;
volatile uint8_t flagsec = 0;
volatile uint8_t muxflag=1;
//volatile uint8_t ia =  0;
volatile uint16_t lt = 0;

// display mode
uint8_t displaymode = MODE_TIME;

// contains the time
static uint8_t sec=0;
static uint8_t min=0;
static uint8_t hour=0;


ISR(TIMER1_COMPA_vect, ISR_NOBLOCK)
{
    flag++;
    if (!(flag &0x1f)) {
        muxflag=1;
        lt++;
        if (lt==250) { // 1sec 
            lt=0;
            flagsec=1;
        }
    }
    
    if (displaymode == MODE_TIME)
        irmp_ISR();
}

void readIRCodes() {
    uint16_t i;
    for (i=0;i<11;i++) {
        ir_codes[i] = eeprom_read_byte((uint8_t*) i);
    }
}

// command 0x2 - set IR code
// params: <key-index> <ir-code>
// index 0: "settime"-key
// index 1-10: codes 1,2,3,4,5,6,7,8,9,0
void setIRCode() {
    uint16_t keyindex = usbdata.data.params[0];
    uint8_t code = usbdata.data.params[1];
    
    if (keyindex > 10)
        return;
    
    eeprom_write_byte((uint8_t*) keyindex, code);
    ir_codes[keyindex]=code;
}



// set raw data calculated from display array
void setRawData() {
    uint8_t i;
    for (i=0;i<9;i++) {
        rawdata[i] = digit[display[i]];
    }
}

// set raw data sent per USB
void setRawDataUSB() {
    uint8_t i;
    for (i=0;i<9;i++) {
        rawdata[8-i] = usbdata.data.rawdata[i];
    }
}

// hardware initialization
void hw_init() {
    DDRB=PB_GATE5 | PB_GATE7 | PB_GATE6 | PB_GATE4 | PB_GATE2 | PB_GATE9;
    DDRC=PC_SEGDP | PC_SEGD | PC_SEGC | PC_SEGE | PC_SEGG | PC_SEGB;
    DDRD=PD_SEGF | PD_SEGA | PD_FILDRV | PD_GATE1 | PD_GATE3 | PD_GATE8;

    PORTB = PB_IR;  // pup enable

    // disable all gates
    PORTD |= PD_FILDRV | PD_GATE1 | PD_GATE3 | PD_GATE8;
    PORTB |= PB_GATE5 | PB_GATE7 | PB_GATE6 | PB_GATE4 | PB_GATE2 | PB_GATE9;

    // TODO: tidy up
    TCCR1A = 0x00;
    TCCR1B = 0x09; // (0100) CTC, clk/1
    TCNT1 = 0;
    OCR1A = 999; // 16E6 / 1066 ~ 15kHz
    TIMSK1 = 0x02; // Output compare A match interrupt enable

    uint8_t wgm = 0b010;
    
    TCCR0A = (0b01 << COM0B0) | ((wgm & 0b011) << WGM00);  // toggle OCB0 on compare match, CTC Mode 010
    TCCR0B = (0b010 << CS00) |  ((wgm & 0b100) << WGM02);   // clkdiv / 8 = 2MHz
    TCNT0 = 0;
    OCR0B = 24; // about 40kHz filament switching frequency
    OCR0A = 24;
    
    
    sei();

}

// command 0x01 - set time per USB
void setTimeUSB() {
    uint8_t newhour = usbdata.data.params[0];
    uint8_t newmin = usbdata.data.params[1];
    uint8_t newsec = usbdata.data.params[2];

#ifdef HOUR24
    if (newhour > 23)
        return;
#else
    if (newhour < 1 || newhour > 12)
        return;
#endif
    
    if (newmin > 59 || newsec > 59)
        return;
    
    hour = newhour;
    min = newmin;
    sec = newsec;
}    

static IRMP_DATA irmp_data;


#define USEUSB
int main(void)
{
uchar   i;

#ifdef USEUSB
    wdt_enable(WDTO_1S);
    odDebugInit();
    usbInit();
    usbDeviceDisconnect();  /* enforce re-enumeration, do this while interrupts are disabled! */
    i = 0;
    while(--i){             /* fake USB disconnect for > 250 ms */
        wdt_reset();
        _delay_ms(1);
    }
    usbDeviceConnect();
#endif    
    irmp_init();
    sei();
    setRawData();
    hw_init();

    readIRCodes();
    
    uint8_t mode = 0;
    uint8_t cursor = 7;
    uint8_t blink = 0;
    uint8_t muxgate = 0;
    
    for(;;){                /* main event loop */
#ifdef USEUSB        
        wdt_reset();
        usbPoll();
#endif      
        if (usbreceived) {
            if (usbdata.data.cmd) {
                switch (usbdata.data.cmd) {
                    // nothing to do
                    case 0x00:
                        break;
                    case 0x01:
                        setTimeUSB();
                        break;
                    case 0x02:  // raw segment display USB
                        if (usbdata.data.params[0] == 0) {
                            displaymode = MODE_TIME;
                        } else if (usbdata.data.params[0] == 1) {
                            displaymode = MODE_RAWUSB;
                        }
                        break;
                    case 0x03:
                        setIRCode();
                        break;
                    default:
                        ;
                        // nothing
                }
                usbdata.data.cmd = 0;
            }
            usbreceived=0;
        }
#ifdef USEUSB        
        wdt_reset();
        usbPoll();
#endif      
        
        if (irmp_get_data(&irmp_data)) {
//            usbPoll();
            ir_code = (uint8_t) (irmp_data.command & 0xff);
            ir_count++;
            
            uint8_t cmd = irmp_data.command & 0xff;
            if (!irmp_data.flags & IRMP_FLAG_REPETITION) {
                uint8_t num = 0xff;
                uint8_t j;
                for (j=0;j<10;j++) {
                    if (ir_codes[1+j] == cmd) {
                        num = (j+1) % 10;
                    }
                }
             
                if (num >= 0 && num <= 9 && mode==1) {
                    // validate input
                    uint8_t valid=1;
                    switch (cursor) {
                        case 7: // 0,1,2 allowed
                            if (num>2)
                                valid=0;
                            break;
                        case 6:
#ifdef HOUR24                            
                            if ((display[7]*10+num)>23)
#else
                            if ((display[7]*10+num)>12 || (display[7]*10+num)<1)
#endif                                
                                valid=0;
                            break;
                            
                        case 4:
                            if (num>5)
                                valid=0;
                            break;
                        case 3:
                            if ((display[4]*10+num)>59)
                                valid=0;
                            break;

                        case 1:
                            if (num>5)
                                valid=0;
                            break;
                        case 0:
                            if ((display[1]*10+num)>59)
                                valid=0;
                            break;
                    }
                    if (valid) {
                        display[cursor] = num;
                        switch (cursor) {
                            case 7: cursor=6; break;
                            case 6: cursor=4; break;
                            case 4: cursor=3; break;
                            case 3: cursor=1; break;
                            case 1: cursor=0; break;
                            case 0: 
                                sec=display[1]*10+display[0];
                                min=display[4]*10+display[3];
                                hour=display[7]*10+display[6];                                
                                mode=0; 
                                break;
                        }
                        if (mode)
                            display[cursor]=0x10;
                    }
                }
                
                if (irmp_data.command == ir_codes[0]) {
                    if (!mode) {
                        mode=1;
                        display[0]=0x12;
                        display[1]=0x12;
                        display[2]=0x11;
                        display[3]=0x12;
                        display[4]=0x12;
                        display[5]=0x11;
                        display[6]=0x12;
                        display[7]=0x10;
                        cursor=7;
                    }
                }
            }
        }
#ifdef USEUSB        
        wdt_reset();
        usbPoll();
#endif      
        
        if (flagsec) {
            flagsec=0;
            blink=1-blink;
            if (mode) {
                if (blink & 0x1) {
                    display[cursor] = 0x12;
                } else {
                    display[cursor] = 0x10;
                }
            } else if (!mode) {
                if (blink & 0x1) {
                    display[8] = 0x10;
                } else {
                    display[8] = 0x13;
                    
                }
            }
            
            if (!blink) {
                sec++;
                if (sec>=60) {
                    sec=0;
                    min++;
                    if (min>=60) {
                        min=0;
                        hour++;
#ifdef HOUR24                        
                        if (hour>=24) {
                            hour=0;
                        }
#else
                        if (hour>12) {
                            hour=1;
                        }
#endif
                    }
                }
                if (!mode) {
                    display[0] = sec % 10;
                    display[1] = sec / 10;
                    display[3] = min % 10;
                    display[4] = min / 10;
                    display[6] = hour % 10;
                    display[7] = hour / 10;
                }
            }
            
        }
#ifdef USEUSB        
        wdt_reset();
        usbPoll();
#endif      
        
        if (muxflag) {
            muxflag=0;
            muxgate++;
            if (muxgate==9)
                muxgate=0;

                PORTD |= PD_GATE1 | PD_GATE3 | PD_GATE8;
                PORTB |= PB_GATE5 | PB_GATE7 | PB_GATE6 | PB_GATE4 | PB_GATE2 | PB_GATE9;
                PORTC |= PC_SEGDP | PC_SEGD | PC_SEGC | PC_SEGE | PC_SEGG | PC_SEGB;
                PORTD |= PD_SEGF | PD_SEGA;  

                switch (muxgate) {
                
                case 0:
                    PORTD &= ~PD_GATE1;
                    break;
                case 1:
                    PORTB &= ~PB_GATE2;
                    break;
                case 2:
                    PORTD &= ~PD_GATE3;
                    break;
                case 3:
                    PORTB &= ~PB_GATE4;
                    break;
                case 4:
                    PORTB &= ~PB_GATE5;
                    break;
                case 5:
                    PORTB &= ~PB_GATE6;
                    break;
                case 6:
                    PORTB &= ~PB_GATE7;
                    break;
                case 7:
                    PORTD &= ~PD_GATE8;
                    break;
                case 8:
                    PORTB &= ~PB_GATE9;
                    break;
            }
            if (displaymode == MODE_TIME)
                setRawData();
            else
                setRawDataUSB();
            
            if (rawdata[muxgate] & SEGF)
                PORTD &= ~PD_SEGF;
            if (rawdata[muxgate] & SEGG)
                PORTC &= ~PC_SEGG;
            if (rawdata[muxgate] & SEGE)
                PORTC &= ~PC_SEGE;
            if (rawdata[muxgate] & SEGD)
                PORTC &= ~PC_SEGD;
            if (rawdata[muxgate] & SEGC)
                PORTC &= ~PC_SEGC;
            if (rawdata[muxgate] & SEGA)
                PORTD &= ~PD_SEGA;
            if (rawdata[muxgate] & SEGB)
                PORTC &= ~PC_SEGB;
            if (rawdata[muxgate] & SEGDP)
                PORTC &= ~PC_SEGDP;
        }            
    }
    return 0;
}

/* ------------------------------------------------------------------------- */
